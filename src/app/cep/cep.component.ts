import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-cep',
  templateUrl: './cep.component.html',
  styleUrls: ['./cep.component.css']
})
export class CepComponent implements OnInit {

  cep: string = ''

  constructor() { }

  ngOnInit() {

  }

  cepBind(event: any){
      this.cep = event.target.value
  }

}
