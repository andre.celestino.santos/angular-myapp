import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

home: string = "Home"
cep: string = "CEP"

  constructor() { }

  ngOnInit() {
  }

}
