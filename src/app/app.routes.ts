import {Routes} from '@angular/router'
import {CepComponent} from './cep/cep.component'
import {HomeComponent} from './home/home.component'

export const ROUTES: Routes = [
  {path: '', component: HomeComponent},
  {path: 'cep', component: CepComponent}
]
